import React, { useEffect, useState } from 'react';
import "./main.css";
import {useSelector, useDispatch} from 'react-redux';
// import {all} from '../redux/actions/index'

function Input() {
    const[inputArr, setInputArr] = useState([]);
    const[all, setAll] = useState(0);
    const[Completed, setCompleted] = useState(0);
    const[showAll, setShowAll] = useState(true);
    const[showActive, setShowActive] = useState(false);
    const[completedTask, setCompletedTask] = useState(false);
    const[toggle, setToggle] = useState(true);

    const dispatch = useDispatch();
     const count = useSelector((state) => state.todoProp.all)
    // let arr = useSelector((state) => state.todoProp.inputArr);

    function handleInput(e){
        if (e.key === 'Enter') {
            setInputArr([...inputArr, {id:inputArr.length, value : e.target.value, isTrue:true}])
            // dispatch(all(e.target.value))
            // console.log("this is from arr", arr.toString());
            console.log("type of arr is", typeof(arr));
            console.log("this is from arr and count is", count);
            e.target.value = ""
            // console.log(e.target.value);
            // console.log(inputArr)
           
        }
    }

    function handleComplete(e, id){
        if(e.target.checked && inputArr[id].isTrue){
            setCompleted(Completed+1)
            // console.log(e.target.checked)
            inputArr[id].isTrue = false;
        }
        else if(!inputArr[id].isTrue){
            setCompleted(Completed-1)
            inputArr[id].isTrue = true;
        }
    }

    function allTaskHandler() {
        setShowAll(true);
        setShowActive(false);
        setCompletedTask(false);
        
    }
    
    function activeTaskHandler() {
        setShowAll(false);
        setShowActive(true);
        setCompletedTask(false);
    }

    function completedTaskHandler() {

        setShowAll(false);
        setShowActive(false);
        setCompletedTask(true);
    }


    function deleteFromList(id) {
        let tempArr = inputArr.filter((data, idx)=>{
            if(idx === id && inputArr[id].isTrue === false){
                setCompleted(Completed-1);
            }
            return idx !== id;
        })
        // console.log(tempArr);
        setInputArr(tempArr)
    }

    function clearCompletedeHandler() {
        let tempArr = inputArr.filter((data, id)=>{
            if(inputArr[id].isTrue === false){
                setCompleted(0);
            }
            return inputArr[id].isTrue != false;
        })
        // console.log(tempArr);
        setInputArr(tempArr)
    }

    function toggleHandler() {
        setToggle(toggle ? false : true);

    }
    
    useEffect(()=>{
        setAll(inputArr.length-Completed)
    })

    return ( 
        <div>
           <section className="todoapp">
                <header>
                    <input className="new-todo"  placeholder="What needs to be done?" onKeyDown={(e) => handleInput(e)}></input> 
                </header>

                <section className="main">
                    <input id="toggle-all" className="toggle-all" type="checkbox" />

                    
                    {(showAll && inputArr.length > 0) && <label htmlFor="toggle-all" onClick={() => toggleHandler()}></label>}
                    {(showActive && inputArr.length - Completed >= 0) && <label htmlFor="toggle-all" onClick={() => toggleHandler()}></label>}
                    {(completedTask && Completed > 0) && <label htmlFor="toggle-all" onClick={() => toggleHandler()}></label>}
                     
                    <ul className="todo-list">
                    {
                     (showAll && toggle) &&   inputArr.map((data, idx)=>{ 
                            return(
                                <li key={idx} className={`${!data.isTrue ? "completed" :""}`}>
                                    <div className="view">
                                        <input className="toggle" type="checkbox" readOnly={true} checked={data.isTrue ? "" : true} onClick={(e) => handleComplete(e, idx)}></input>
                                        <label>{data.value}</label>
                                        <button className="destroy" onClick={(e)=> deleteFromList(idx)}></button>
                                    </div>
                                </li>
                            )
                        })
                    }

                   
                    { // active
                     (showActive && toggle) &&   inputArr.map((data, idx)=>{
                         if(data.isTrue)
                            return(
                                <li key={idx} className={`${!data.isTrue ? "completed" :""}`}>
                                    <div className="view">
                                        <input className="toggle" type="checkbox" onClick={(e) => handleComplete(e, idx)}></input>
                                        <label>{data.value}</label>
                                        <button className="destroy" onClick={()=> deleteFromList(idx)}></button>
                                    </div>
                                </li>
                            )
                        })
                    }


                   
                    { // completed task
                     (completedTask && toggle) &&   inputArr.map((data, idx)=>{
                         if(!data.isTrue)
                            return(
                                <li key={idx} className={`${!data.isTrue ? "completed" :""}`}>
                                    <div className="view">
                                        <input className="toggle" type="checkbox" checked={true} readOnly={true} onClick={(e) => handleComplete(e, idx)}></input>
                                        <label>{data.value}</label>
                                        <button className="destroy" onClick={()=> deleteFromList(idx)}></button>
                                    </div>
                                </li>
                            )
                        })
                    }
                     </ul>
                </section>
                <footer className="footer">
                    <span className="todo-count">
                        <strong>{all}</strong>
                        <span> </span>
                        <span>items</span>
                        <span> left</span>   
                    </span>
                    <ul className="filters">
                        <li>
                            <a href="#/" onClick={() => allTaskHandler()} className={showAll ? "selected":""}>All</a>
                        </li>
                        <span> </span>
                        <li>
                            <a href="#/" onClick={activeTaskHandler} className={showActive ? "selected":""}>Active</a>
                        </li>
                        <span> </span>
                        <li>
                            <a href="#/" onClick={completedTaskHandler} className={completedTask ? "selected":""}>Completed</a>
                        </li>
                    </ul>
                   {Completed > 0 && <button className="clear-completed" onClick={clearCompletedeHandler}>Clear Completed</button>}
                </footer>
               
           </section>
        </div>
     );
}

export default Input;