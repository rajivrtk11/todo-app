import {combineReducers} from 'redux';
import {todoFunction} from './todoFunction';

export default combineReducers({
    todoProp : todoFunction
})