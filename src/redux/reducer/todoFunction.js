const data = {
    intputArr:[],
    all:0,
    completed:0,
    showAll:true,
    showActive:false,
    completedTask:false,
    toggle:true
}

export function todoFunction(state = data, action) {
    switch(action.type){
        case "ALL": return {...state, intputArr:[...state.intputArr, action.payload], all: state.intputArr.length - state.completed};
        case "ACTIVE" : return {...state, isRunning:false, isStart:false};
        // case "COMPLETED" : return {...state, lapArr:[...state.lapArr, state.sec]};
        case "CLEARCOMPLETED" : return{...state, sec:state.sec+10};
        case "REMOVE": return {isStartBtn:false, lapArr:[], sec:0, isRunning:false};
        case "RESUME": return {...state,isStart:true, isRunning:true };
        default : return state;
    }
}