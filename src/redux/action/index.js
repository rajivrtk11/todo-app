export function all(val) {
    return{
        type:"ALL",
        payload:val
    }
}

export function active() {
    return{
        type:"ACTIVE"
    }
}

export function completed(){
    return{
        type:"COMPLETED"
    }
}


export function clearCompleted() {
    return{
        type: "CLEARCOMPLETED",
    }
}

export function remove(){
    return{
        type: "REMOVE",
    }
}



